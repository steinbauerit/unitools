<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SteinbauerIT.Unitools',
            'Iframe',
            [
                'Calendar' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'Calendar' => 'create, update, delete'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    iframe {
                        iconIdentifier = unitools-plugin-iframe
                        title = LLL:EXT:unitools/Resources/Private/Language/locallang_db.xlf:tx_unitools_iframe.name
                        description = LLL:EXT:unitools/Resources/Private/Language/locallang_db.xlf:tx_unitools_iframe.description
                        tt_content_defValues {
                            CType = list
                            list_type = unitools_iframe
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'unitools-plugin-iframe',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:unitools/Resources/Public/Icons/user_plugin_iframe.svg']
			);
		
    }
);
