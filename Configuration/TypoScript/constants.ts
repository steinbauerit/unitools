
plugin.tx_unitools_iframe {
    view {
        # cat=plugin.tx_unitools_iframe/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:unitools/Resources/Private/Templates/
        # cat=plugin.tx_unitools_iframe/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:unitools/Resources/Private/Partials/
        # cat=plugin.tx_unitools_iframe/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:unitools/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_unitools_iframe//a; type=string; label=Default storage PID
        storagePid =
    }
}
