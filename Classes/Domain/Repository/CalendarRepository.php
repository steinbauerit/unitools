<?php
namespace SteinbauerIT\Unitools\Domain\Repository;

/***
 *
 * This file is part of the "Unitools" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Patric Eckhart <mail@patriceckhart.com>, STEINBAUER IT
 *
 ***/

/**
 * The repository for Calendars
 */
class CalendarRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
