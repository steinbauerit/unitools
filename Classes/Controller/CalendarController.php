<?php
namespace SteinbauerIT\Unitools\Controller;

/***
 *
 * This file is part of the "Unitools" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Patric Eckhart <mail@patriceckhart.com>, STEINBAUER IT
 *
 ***/

/**
 * CalendarController
 */
class CalendarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * calendarRepository
     *
     * @var \SteinbauerIT\Unitools\Domain\Repository\CalendarRepository
     * @inject
     */
    protected $calendarRepository = null;

    /**
     * action list
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @return void
     */
    public function listAction()
    {
        $calendars = $this->calendarRepository->findAll();
        $this->view->assign('calendars', $calendars);
    }

    /**
     * action show
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @return void
     */
    public function showAction(\SteinbauerIT\Unitools\Domain\Model\Calendar $calendar)
    {
        $this->view->assign('calendar', $calendar);
    }

    /**
     * action new
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * action create
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @return void
     */
    public function createAction(\SteinbauerIT\Unitools\Domain\Model\Calendar $newCalendar)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->calendarRepository->add($newCalendar);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @ignorevalidation $calendar
     * @return void
     */
    public function editAction(\SteinbauerIT\Unitools\Domain\Model\Calendar $calendar)
    {
        $this->view->assign('calendar', $calendar);
    }

    /**
     * action update
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @return void
     */
    public function updateAction(\SteinbauerIT\Unitools\Domain\Model\Calendar $calendar)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->calendarRepository->update($calendar);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param SteinbauerIT\Unitools\Domain\Model\Calendar
     * @return void
     */
    public function deleteAction(\SteinbauerIT\Unitools\Domain\Model\Calendar $calendar)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->calendarRepository->remove($calendar);
        $this->redirect('list');
    }
}
