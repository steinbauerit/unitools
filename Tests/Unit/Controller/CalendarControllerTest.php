<?php
namespace SteinbauerIT\Unitools\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Patric Eckhart <mail@patriceckhart.com>
 */
class CalendarControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \SteinbauerIT\Unitools\Controller\CalendarController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\SteinbauerIT\Unitools\Controller\CalendarController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllCalendarsFromRepositoryAndAssignsThemToView()
    {

        $allCalendars = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $calendarRepository = $this->getMockBuilder(\SteinbauerIT\Unitools\Domain\Repository\CalendarRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $calendarRepository->expects(self::once())->method('findAll')->will(self::returnValue($allCalendars));
        $this->inject($this->subject, 'calendarRepository', $calendarRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('calendars', $allCalendars);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenCalendarToView()
    {
        $calendar = new \SteinbauerIT\Unitools\Domain\Model\Calendar();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('calendar', $calendar);

        $this->subject->showAction($calendar);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenCalendarToCalendarRepository()
    {
        $calendar = new \SteinbauerIT\Unitools\Domain\Model\Calendar();

        $calendarRepository = $this->getMockBuilder(\SteinbauerIT\Unitools\Domain\Repository\CalendarRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $calendarRepository->expects(self::once())->method('add')->with($calendar);
        $this->inject($this->subject, 'calendarRepository', $calendarRepository);

        $this->subject->createAction($calendar);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenCalendarToView()
    {
        $calendar = new \SteinbauerIT\Unitools\Domain\Model\Calendar();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('calendar', $calendar);

        $this->subject->editAction($calendar);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenCalendarInCalendarRepository()
    {
        $calendar = new \SteinbauerIT\Unitools\Domain\Model\Calendar();

        $calendarRepository = $this->getMockBuilder(\SteinbauerIT\Unitools\Domain\Repository\CalendarRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $calendarRepository->expects(self::once())->method('update')->with($calendar);
        $this->inject($this->subject, 'calendarRepository', $calendarRepository);

        $this->subject->updateAction($calendar);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenCalendarFromCalendarRepository()
    {
        $calendar = new \SteinbauerIT\Unitools\Domain\Model\Calendar();

        $calendarRepository = $this->getMockBuilder(\SteinbauerIT\Unitools\Domain\Repository\CalendarRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $calendarRepository->expects(self::once())->method('remove')->with($calendar);
        $this->inject($this->subject, 'calendarRepository', $calendarRepository);

        $this->subject->deleteAction($calendar);
    }
}
