<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'SteinbauerIT.Unitools',
            'Iframe',
            'MUL Unitools - iFrame'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('unitools', 'Configuration/TypoScript', 'MUL Unitools');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_unitools_domain_model_calendar', 'EXT:unitools/Resources/Private/Language/locallang_csh_tx_unitools_domain_model_calendar.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_unitools_domain_model_calendar');

    }
);
